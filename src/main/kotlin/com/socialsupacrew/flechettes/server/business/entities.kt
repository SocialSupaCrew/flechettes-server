package com.socialsupacrew.flechettes.server.business

data class News(
    val id: Long,
    val date: String,
    val title: String,
    val subtitle: String,
    val image: String
)
