package com.socialsupacrew.flechettes.server.client

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.Instant

data class NewsJson(
    @JsonProperty("id")
    val id: Long,

    @JsonProperty("date")
    val date: Instant,

//    @Size(min = MIN_SIZE_NAME, max = MAX_SIZE_NAME)
    @JsonProperty("title")
    val title: String,

    @JsonProperty("subtitle")
    val subtitle: String,

    @JsonProperty("image")
    val image: String
)
