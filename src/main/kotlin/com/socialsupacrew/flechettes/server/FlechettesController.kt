package com.socialsupacrew.flechettes.server

import com.socialsupacrew.flechettes.server.business.News
import com.socialsupacrew.flechettes.server.client.NewsJson
import com.socialsupacrew.flechettes.server.data.NewsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.time.Instant

@RestController
class FlechettesController
@Autowired constructor(
    private val newsService: NewsService
) {

    @RequestMapping(path = [Path.news], method = [RequestMethod.GET])
    fun getNews(): List<NewsJson> {
        return newsService.findAll()
            .map(this::toJson)
    }

    internal fun toJson(news: News) =
        NewsJson(news.id, Instant.parse(news.date), news.title, news.subtitle, news.image)
}
