package com.socialsupacrew.flechettes.server.data

import com.socialsupacrew.flechettes.server.business.News
import org.springframework.stereotype.Component


@Component
class NewsConverter {
    fun toBusiness(news: NewsDb): News {
        return News(news.id, news.date, news.title, news.subtitle, news.image)
    }
}
