package com.socialsupacrew.flechettes.server.data

import com.socialsupacrew.flechettes.server.business.News
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Component

@Component
class NewsService(
    private val newsRepository: NewsRepository,
    private val converter: NewsConverter
) {

    fun findAll(): List<News> = newsRepository.findAllByOrderByDateDesc()
        .map(converter::toBusiness)
}

@Component
interface NewsRepository : CrudRepository<NewsDb, Long> {
    fun findAllByOrderByDateDesc(): Iterable<NewsDb>
}
