package com.socialsupacrew.flechettes.server.data

import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id

@Entity(name = "news")
data class NewsDb(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0,

    @Column(name = "date", nullable = false)
    val date: String = "",

    @Column(name = "title", nullable = false)
    val title: String = "",

    @Column(name = "subtitle", nullable = true)
    val subtitle: String = "",

    @Column(name = "image", nullable = true)
    val image: String = ""
)

